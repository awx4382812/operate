# 使用 CentOS 7 作為基礎鏡像
FROM centos:7

# 更新系統，安裝 EPEL 倉庫、開發工具、UnixODBC 開發庫、GCC、Python 3.6 的開發庫和 Python3 的 pip
RUN yum -y update && \
    yum -y install epel-release && \
    yum -y groupinstall "Development Tools" && \
    yum -y install unixODBC-devel gcc python36-devel python3-pip && \
    yum clean all

# 升級 setuptools、wheel 和 pip3
RUN pip3 install --upgrade setuptools wheel pip

# 安裝 Python 套件
RUN pip3 install pycryptodome fastapi aioodbc SQLAlchemy pyodbc lxml==4.9.2 PyYAML==4.2b4 requests==2.27.1 urllib3==1.26.15  uvicorn

# 安裝 Microsoft's ODBC Driver for SQL Server
RUN curl -O https://packages.microsoft.com/keys/microsoft.asc && \
    rpm --import microsoft.asc && \
    curl https://packages.microsoft.com/config/rhel/7/prod.repo > /etc/yum.repos.d/mssql-release.repo && \
    ACCEPT_EULA=Y yum install msodbcsql17 -y && \
    odbcinst -q -d -n "ODBC Driver 17 for SQL Server"
